<?php


namespace App\Exercise;

class Device
{

    public $id;
    public $label;
    public $color;
    public $ip;
    public $activated;

    public function __construct(
        int $id,
        string $label,
        string $color,
        string $ip,
        bool $activated
    ) {
        $this->id = $id;
        $this->label = $label;
        $this->color = $color;
        $this->ip = $ip;
        $this->activated = $activated;
    }

    public function switchActivate(): void
    { }

    public function renderHTML()
    { }
}
