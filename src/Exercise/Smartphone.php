<?php

namespace App\Exercise;

class Smartphone extends Device
{

    public $phoneNumber;
    public $os;
    
  

    public function __construct(string $phoneNumber, 
    string $os, int $id, string $label, string $color, string $ip, bool $activated )
    {
        parent::__construct($id, $label, $color, $ip, $activated);
        $this->phoneNumber = $phoneNumber;
        $this->os = $os;     
    }

    public function call(string $phoneNumber ):void
    {
        
        
    }
}