<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>IoT</title>
<style>
form {
    width: 350px;
    Height:490px;
    box-shadow: 0px 1px 4px gray;
    padding: 10px;
    margin: 50px;
    align-items: center;
}

input {
    margin-bottom: 5px;
}
</style>

</head>

<body>
    <form action="exercise-IoT.php" method="POST">
        <div class="form-row">
            <div class="form-group col-12">
                <label for="product-id">ID</label>
                <input type="number" class="form-control" id="product-id" placeholder="ID Number">
            </div>
            <div class="form-group col-12">
                <label for="product-label">Product_Label</label>
                <input type="text" class="form-control" id="product-label" placeholder="Label">
            </div>
            <div class="form-group col-12">
                <label for="color">Color</label>
                <input type="color" class="form-control" id="product-color">
            </div>
            <div class="form-group col-12">
                <label for="ip_address">IP_Address</label>
                <input type="text" class="form-control" id="ip-address">
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" id="gridCheck">
                <label class="form-check-label" for="gridCheck">
                    Activation
                </label>
            </div>
        </div>
        <button type="submit" class="btn btn-primary" id="submit">Submit</button>
    </form>


    <?php

    use App\Exercise\Device;

    use App\Exercise\Smartphone;

    use App\Exercise\IoT;

    require_once 'vendor/autoload.php';

    $device1 = new Device(1, "Toaster", "black", "192.145.1.1", true);

    // $device2= new Device(2,"Fridge","white", "160.147.1.2");

    // $device3= new Device(3,"Oven","black", "197.211.1.3");

    // $device4= new Device(4,"Mixer","black", "131.140.1.4");

    // echo $device->addDevice();

    $iot1 = new IoT();

    
    $iot1->addDevice($device1);

    var_dump($iot1);
    